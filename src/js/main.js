document.documentElement.className = 'js';
import "babel-polyfill";
import 'picturefill';

import './modules/FontLoader';
import './modules/Poster';
import './modules/FloatingLabel';

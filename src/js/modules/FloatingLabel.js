const labels = document.querySelectorAll('[data-placeholder]');


function addFocus(_label) {
    _label.classList.add('is-focused');
}

function removeFocus(_label) {
    _label.classList.remove('is-focused');
}

function bindField(_field, _label) {
    _field.addEventListener('focus', function() {
        addFocus(_label);
    });

    _field.addEventListener('blur', function() {
        if(_field.value !== '') return;
        removeFocus(_label);
    });
}


function cutsMustard() {
    return 'matchMedia' in window && 'classList' in document.body && 'addEventListener' in document.body;
}

if(cutsMustard() && labels) {

    [...labels].forEach(label => {
        var id = label.getAttribute('data-placeholder'),
            field = document.getElementById(id);

        removeFocus(label);
        bindField(field, label);

        if(field.value !== '') {
            addFocus(label);
        }
    })

}
